const express = require("express");
const createError = require("http-errors");
const router = express.Router();
const { UserQuery } = require("../database/schemas/user");
const jsonWebToken = require("jsonwebtoken");

// TODO: reduce "const UserQuery" allocations

router.route("/register").post((req, res) => {
  registerUser(req, res);
});

router.route("/login").post((req, res) => {
  login(req, res);
});

router.route("/loginFB").post((req, res) => {
  loginFB(req, res);
});

router.route("/preferences").post((req, res) => {
  const userQuery = new UserQuery();
  let token = req.body.data;
  console.log(userQuery.decodeToken(token));
  let user = userQuery.decodeToken(token);
  // take preferences

  //if they are not present return empty object

  userQuery.findOne({ email: user.email }).then((DBresponse) => {
    if (typeof DBresponse.preferences === "undefined") {
      res.status(200).json({ preferences: [] });
    } else {
      res.status(200).json({ preferences: DBresponse.preferences });
    }
  });
});

//Update preferences with PUT - (Update/Replace) CRUD
router.route("/preferences").put((req, res) => {
  const userQuery = new UserQuery();
  let token = req.body.token;
  let user = userQuery.decodeToken(token);

  userQuery
    .updateOne({ email: user.email }, { preferences: req.body.preferences })
    .then((DBresponse) => {
      res.status(200).json(DBresponse);
    });

  userQuery.findOne({ email: user.email }).then((DBresponse) => {
    console.log(DBresponse);
  });
});

async function registerUser(req, res) {
  const userQuery = new UserQuery();
  if (await userQuery.register(req)) {
    res.sendStatus(200);
  } else {
    res.sendStatus(700);
  }
}

async function login(req, res) {
  const userQuery = new UserQuery();
  let result = await userQuery.login(req);

  if (result != false) {
    res.status(200).json(result);
  } else {
    res.sendStatus(400);
  }
}

async function loginFB(req, res) {
  const userQuery = new UserQuery();
  let result = await userQuery.loginFB(req);

  if (result != false) {
    res.status(200).json(result);
  } else {
    res.sendStatus(400);
  }
}

module.exports = router;
