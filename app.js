// app.js

const { gameServer } = require("./services/gameServer");

const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
// const fs = require("fs");
const database = require("./database/database");

// Create Express app
const app = express();

// const options = {
//   cert: fs.readFileSync("certs/certificate.crt"),
//   key: fs.readFileSync("certs/private.key"),
// };

// const server = require("http").createServer(app);

const rateLimit = require("express-rate-limit");
// const server = require("https").createServer(options,app);

//Io websocket
const socketIO = require("socket.io");

const RTCMultiConnectionServer = require("rtcmulticonnection-server");
const config = require('./configuration.json');

//File access
const path = require("path");
const WouldYouRather = require("./services/games/wouldRather").WouldYouRather;

//Routes

const userRoutes = require("./routes/userRoute");
const wouldRatherRoute = require("./services/games/wouldRather").router;

// Set helmet
app.use(helmet());
//Use json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors({
    credentials: true,
    origin: true,
  })
);

//configure limiter
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
});
app.use(limiter);

//Setting secure cookie settings
const session = require("express-session");
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: "youruniquesecret", // secret string used in the signing of the session ID that is stored in the cookie
    name: "RekiKy", // set a unique name to remove the default connect.sid
    cookie: {
      httpOnly: true, // minimize risk of XSS attacks by restricting the client from reading the cookie
      // secure: true, // only send cookie over https
      maxAge: 60000 * 60 * 24, // set cookie expiry length in ms
    },
  })
);

// Connect to DB

database.connect().then((res) => {
  module.exports.database = res;
});

/// specify routes
app.use("/users", userRoutes);
////app.use("/bar",arRoutes);
app.use("/would", wouldRatherRoute);

//to serve files to clients
app.use(express.static(path.join(__dirname, "public/build")));

const PORT = process.env.PORT || 4000;
app.set('port', PORT);

const server = app
    .use((req, res) => res.sendFile('index.html', {root: path.join(__dirname, 'public/build/')}))
    .listen(PORT, () => console.log(`Listening on ${PORT}`));

const io = socketIO(server);

io.on("connection", function (socket) {
    RTCMultiConnectionServer.addSocket(socket);

    socket.on("open-room", (data) => {
        console.log("open > ",data.sessionid );
        gameServer.initSession(socket,data.sessionid)

    });

    socket.on("join-room", (data) => {
        console.log("join > ",data.sessionid );
        gameServer.joinSession(socket,data.sessionid)

    });

    const params = socket.handshake.query;

    if (!params.socketCustomEvent) {
        params.socketCustomEvent = "custom-message";
    }

    socket.on(params.socketCustomEvent, function (message) {
        socket.broadcast.emit(params.socketCustomEvent, message);
    });
});

