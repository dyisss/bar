const GameSession = require("./gameSession");

class GameServer {
  constructor() {
    this.listOfSessions = []; //todo: remove session when every socket leaves and change host when one socket leaves
  }

  initSession(socket, sessionId) {
    const gameSession = new GameSession(sessionId);
    this.listOfSessions.push(gameSession);

    gameSession.setListeners(socket);
    socket.join(sessionId);
  }

  joinSession(socket, sessionId) {
    this.listOfSessions.forEach((gameSession) => {
      if (gameSession.sessionId === sessionId) {
        gameSession.setListeners(socket);
        socket.join(sessionId);
      }
    });
  }

  leaveSession(socket, sessionId) {
    //todo remove from this.listOfSession{session}.sockets
    //todo remove/close session if last socket left
    socket.leave(sessionId);
  }
}

const gameServer = new GameServer();

module.exports = { gameServer };
