const { gamesMap } = require("./games/gameManager");

class GameSession {
  constructor(sessionId) {
    this.sockets = [];
    this.currentGameName = null;
    this.game = null;
    this.sessionId = sessionId;
  }

  setListeners(socket) {
    this.sockets.push(socket);

    socket.on("current Game", (callback) => {
      if (this.game) {
        this.game.setListeners(socket);
        callback(null, this.currentGameName);
      } else {
        callback(null, "");
      }
    });

    socket.on("update game on session", () => {
      if (this.game) {
        this.game.setListeners(socket);
      }
    });

    socket.on("selected new Game", (gameName, callback) => {
      if (this.game) {
        this.game.unSetListeners(this.sockets);
        this.game = null;
      }

      this.currentGameName = gameName;

      if (gameName != "") {
        let gameReference = getGame(gameName);
        let newGame = new gameReference(this.sessionId);
        this.game = newGame;

        if (newGame) {
          this.game.setListeners(socket);
        }
      }

      socket.to(this.sessionId).emit("game update", gameName);
      callback(null, this.currentGameName);
    });
  }
}

function getGame(name) {
  const returnGame = gamesMap.get(name);
  if (typeof returnGame == "undefined") {
    return null;
  } else {
    return returnGame;
  }
}

module.exports = GameSession;
