const { WouldYouRather } = require("./wouldRather");
const { WasNotMe } = require("./wasNotMe");

const gamesMap = new Map();

// Place the name you expect from front-end

gamesMap.set("WasNotMe", WasNotMe);
gamesMap.set("WouldYouRather", WouldYouRather);

module.exports = { gamesMap };
