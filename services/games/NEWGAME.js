const Game = require("./Game");

class ExampleGame extends Game {
  constructor(sessionId) {
    super(sessionId);
    this.gameName = "Example Game Name";
  }

  setListeners(socket) {}

  unSetListeners(listOfSockets) {}
}

module.exports = { ExampleGame };
