class Game {
  constructor(sessionId) {
    this.sessionId = sessionId;
    this.gameName = "";
  }

  setListeners(socket) {
    throw "Did not Override setListeners function";
  }

  unSetListeners(listOfSockets) {
    throw "Did not Override unSetListeners function";
  }

  setGameName() {
    throw "Did not set name of game";
  }
}

module.exports = Game;
