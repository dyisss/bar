const Game = require("./Game");

class WasNotMe extends Game {
  constructor(sessionId) {
    super(sessionId);
    this.lastClicker = "Still No One";
    this.gameName = "WasNotMe";
  }

  setListeners(socket) {
    socket.on("user clicked", (sender_id, callback) => {
      this.lastClicker = sender_id;
      socket.to(this.sessionId).emit("new ClickUpdate", this.lastClicker);
      callback(null, this.lastClicker);
    });

    socket.on("current Clicker", (callback) => {
      callback(null, this.lastClicker);
    });
  }

  unSetListeners(listOfSockets) {
    listOfSockets.forEach((socket) => {
      socket.removeAllListeners("user clicked");
      socket.removeAllListeners("current Clicker");
    });
  }
}

module.exports = { WasNotMe };
