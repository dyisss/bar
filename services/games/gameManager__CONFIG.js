const { WouldYouRather } = require("./wouldRather");
const { WasNotMe } = require("./wasNotMe");
const { ExampleGame } = require("./NEWGAME");

// add your game here to be loaded!

const __CONFIG = [WasNotMe, WouldYouRather, ExampleGame];

module.exports = { __CONFIG };
