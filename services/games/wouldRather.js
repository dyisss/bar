const express = require("express");
const router = express.Router();

const { WouldQuery } = require("../../database/schemas/wouldQuestion");
const Game = require("./Game");

/// Game server used for sockets
class WouldYouRather extends Game {
  constructor(sessionId) {
    super(sessionId);
    this.gameName = "WouldYouRather";
    this.arrayOfRather = [];
    this.answers = [];
    this.question = null;
    this.page = 1;
    this.limit = 20;

    const wQuery = new WouldQuery();
    //TODO:- move this to the query class with appropriate function
    this.mod = wQuery.getModel();
    this.mod
      .paginate({}, { page: this.page, limit: this.limit })
      .then((res) => {
        this.loadQuestions(res.docs);
        this.question = popRandom(this.arrayOfRather);
      });
  }

  loadQuestions(array) {
    array.forEach((ele) => {
      this.arrayOfRather.push(ele);
    });
  }

  setListeners(socket) {
    socket.on("current Question", (callback) => {
      if (!this.question) setTimeout(() => callback(null, this.question), 200);
      else callback(null, this.question);
    });

    socket.on("answer selected", (answer, sender_id, callback) => {
      let option = answer;

      //check for existing reply and replace
      const answerByUserExists = this.answers.find(
        (answer) => answer.user === sender_id
      );
      if (answerByUserExists) {
        this.answers = this.answers.slice().map((answer) => {
          if (answer.user === sender_id) {
            return { user: sender_id, option: option };
          } else return answer;
        });
      } else this.answers.push({ user: sender_id, option: option });

      socket.to(this.sessionId).emit("answers update", this.answers);

      callback(null, this.answers);
    });

    socket.on("next Question", (callback) => {
      if (this.arrayOfRather.length > 1) {
        this.question = popRandom(this.arrayOfRather);
        // console.log("Questions remaining for ",this.sessionId , " >>>>", this.arrayOfRather.length);
      } else {
        this.mod
          .paginate({}, { page: (this.page += 1), limit: this.limit })
          .then((res) => this.loadQuestions(res.docs));
        this.question = popRandom(this.arrayOfRather);
      }

      socket.to(this.sessionId).emit("newQuestion", this.question);

      this.answers = [];

      socket.to(this.sessionId).emit("answers update", this.answers);

      // Use a Node style callback (error, value)
      callback(null, this.question);
    });
  }

  unSetListeners(listOfSockets) {
    listOfSockets.forEach((socket) => {
      socket.removeAllListeners("next Question");
      socket.removeAllListeners("answer selected");
      socket.removeAllListeners("current Question");
    });
  }
}

/*
 *
 * REST API
 */

router.post("/randomQuestion", async function (req, res) {
  let params = req.body;
  let option_1 = params.option_1;
  let option_2 = params.option_2;
  let category = params.category;
  if (option_1 !== "" && option_2 !== "" && category !== "") {
    const doc = new ratherModel({
      option_1: option_1,
      option_2: option_2,
      category: category,
    });

    await doc.save((err, doc) => {
      //todo : looks fishy
      if (err) {
        res.status(201).send({ error: err });
        return console.error(err);
      } else {
        res.status(201).send("Saved");
      }
    });
  } else {
    res.status(404).send("No empty pls");
  }
});

function popRandom(array) {
  //todo
  const object = array[~~(Math.random() * array.length)];
  array.splice(array.indexOf(object), 1);
  return object;
}

module.exports = { router, WouldYouRather };
