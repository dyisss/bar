# Bar

A side project Kyle and Rehan. It is a simulation of a bar(an interactive app).


**To start the server**<br/>
Go to the server directory<br/>

run command `npm i nodemon` <br/>
run `npm install helmet --save`
run command `nodemon app.js` to start the server<br/>
or double-click "start.bat" (windows only) <br/>


</br>

        **Modules**
        

**express**

minimalist web framework for node.

[Read more](https://www.npmjs.com/package/express)

**nodemon**

Nodemon is a tool that helps develop node.js based applications by automatically <br/>
restarting the node application when file changes in the directory are detected. <br/>

[Read more](https://www.npmjs.com/package/nodemon)

**helmet** 

Helmet helps you secure your Express apps by setting various HTTP headers.

[Read more](https://www.npmjs.com/package/helmet)

**cors**

CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.

[Read more](https://www.npmjs.com/package/cors)

**mongoDB**

The official MongoDB driver for Node.js. Provides a high-level API on top of mongodb-core that is meant for end users.

[Read more](https://www.npmjs.com/package/mongodb)



