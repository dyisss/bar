class Queries {
  constructor(collection, schema) {
    let DbReference = require("../app").database;
    this.model = DbReference.model(collection, schema);
  }
  /* Template for all the Queries

    // Model.deleteMany()
    // Model.deleteOne()
    // Model.find()
    // Model.findById()
    // Model.findByIdAndDelete()
    // Model.findByIdAndRemove() // DON'T USE
    // Model.findByIdAndUpdate()
    // Model.findOne()
    // Model.findOneAndDelete()
    // Model.findOneAndRemove() // DON'T USE
    // Model.findOneAndReplace()
    // Model.findOneAndUpdate()
    // Model.replaceOne()
    // Model.updateMany()
    // Model.updateOne()

    */

  /* deleteMany ---> 
   The deleteMany() method returns an object that contains three fields:
        1. n – number of matched documents
        2. ok – 1 if the operation was successful
        3. deletedCount – number of deleted documents
        
        [Not tested]
  */
  async deleteMany(deleteQuery) {
    return this.model.deleteMany(deleteQuery, (err, result) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  /*
  The deleteOne() method returns an object containing three fields.

    1.     n – number of matched documents
    2.     ok – 1 if the operation was successful
    3.     deletedCount – number of documents deletedCount

        [Not tested]

  */

  async deleteOne(deleteQuery) {
    return this.model.deleteOne(deleteQuery, (err, result) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  /*
  The find() method returns an object containing three fields. (Cursor)

    1.     n – number of matched documents
    2.     ok – 1 if the operation was successful
    3.     deletedCount – number of documents deletedCount

    [Not tested]

  */

  async find(query) {
    return this.model.find(query, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  /*
  The findByID() method returns an object that matches the ID

    [Not tested]

  */

  async findById(ID) {
    return this.model.findByID(ID, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  /*
  The findByIdAndDelete() method finds the object that matches the ID and deletes it
    [Not tested]

  */

  async findByIdAndDelete(ID) {
    return this.model.findByID(ID, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  /*
  The findByIdAndUpdate() method find the object that matches the ID and updates it, returning back the updated 
    [Not tested]

  */
  async findByIdAndUpdate(ID, update) {
    return this.model.findByIdAndUpdate(ID, update, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  /*
  The findOne() method finds object based on model and Query and returns it.
    [Not tested]
  */

  async findOne(query) {
    return this.model.findOne(query, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  /*
  The findOneAndDelete() method finds object based on model and Query and deletes it.
    [Not tested]
  */
  async findOneAndDelete(query) {
    return this.model.findOneAndDelete(query, (err, res) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  async findOneAndUpdate(query, update) {
    return this.model.findOneAndDelete(query, update, (err, res) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  async findOneAndReplace(query, replace) {
    return this.model.findOneAndDelete(query, replace, (err, res) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  async findOneAndUpdate(query, update) {
    return this.model.findOneAndDelete(query, update, (err, res) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  async replaceOne(query, replacement) {
    return this.model.replaceOne(query, replacement, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  async updateMany(query, update) {
    return this.model.updateMany(query, update, (err, res) => {
      if (err) throw err;
      else {
        return res;
      }
    });
  }

  async updateOne(query, update) {
    return this.model.updateOne(query, update, (err, result) => {
      if (err) throw err;
      else {
        return result;
      }
    });
  }

  save(doc) {
    doc.save((err) => {
      if (err) throw err;
    });
  }

  getModel() {
    return this.model;
  }
}

module.exports = Queries;
