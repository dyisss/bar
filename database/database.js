const mongoose = require("mongoose");
const url = "mongodb+srv://Shared:ReKiKy@1@rekiky-xhlir.gcp.mongodb.net/mydb";
let connection = undefined;
async function connect() {
  connection = await mongoose.connect(
    url,
    { useUnifiedTopology: true, useNewUrlParser: true },
    (err) => {
      if (err) throw err;
      else {
        console.log("DB conneceted");
      }
    }
  );

  return connection;
}
module.exports = { connect };
