// Would question schema to be created here,...

const Query = require("../queries");
const { Schema } = require("mongoose");
const Paginate = require("mongoose-paginate-v2");

// Define Schema for specific purpose
ratherSchema = new Schema({
    id: Number,
    option_1: String,
    option_2: String,
    category: String
});

ratherSchema.plugin(Paginate);

// Use this file as a template for building models

class WouldQuery extends Query {
    constructor() {
        super("ratherQuestions", ratherSchema);
    }
}
module.exports.WouldQuery = WouldQuery;
