const Query = require("../queries");
const { Schema } = require("mongoose");
const jwt = require("jsonwebtoken");
const jwt_secret = require("../../jwt_config").jwt_secret;
const bcrypt = require("bcrypt");

userSchema = new Schema({
  id: Number,
  email: String,
  password: String,
  firstname: String,
  lastname: String,
  gender: String,
  preferences: Array,
});
class UserQuery extends Query {
  constructor() {
    super("users", userSchema);
  }
  async register(jsonReq) {
    let user = jsonReq.body;
    let password = user.password;
    let firstname = user.firstName;
    let lastname = user.lastName;
    let email = user.email;
    let gender = user.gender;
    let dbres = await this.findOne({ email: email });
    if (dbres === null) {
      bcrypt.genSalt(12, (err, salt) => {
        if (err) throw err;
        else {
          const userModel = this.getModel();
          bcrypt.hash(password, salt, async (err, hash) => {
            let newUser = new userModel({
              email,
              password: hash,
              firstname,
              lastname,
              gender,
            });
            this.save(newUser);
          });
        }
      });
      return true;
    } else {
      return false;
    }
  }

  async login(req) {
    let jsonReq = req.body;
    let email = jsonReq.email;
    let password = jsonReq.password;

    let matchedUser = await this.findOne({ email });
    console.log(matchedUser);

    if (matchedUser != null) {
      return await this.comparePassword(password, matchedUser, bcrypt, jsonReq);
    } else {
      return false;
    }
  }

  async loginFB(req) {
    let jsonReq = req.body;
    console.log(jsonReq);
    let matchedUser = await this.findOne({ email: jsonReq.email });
    console.log(matchedUser);

    if (matchedUser == null) {
      this.registerFB(jsonReq);
    }

    return this.bundleResponse(jsonReq, jsonReq.name);
  }

  async registerFB(jsonReq) {
    let email = jsonReq.email;
    let firstname, lastname;

    let names = jsonReq.name.split(" ");
    firstname = names[0];
    lastname = names[names.length - 1];
    const userModel = this.getModel();
    let newUser = new userModel({
      email,
      password: "",
      firstname,
      lastname,
      gender: "unknown",
    });
    this.save(newUser);
  }

  async comparePassword(password, matchedUser, bcrypt, jsonReq) {
    let success = await new Promise((resolve, reject) => {
      bcrypt.compare(password, matchedUser.password, (err, compareResult) => {
        if (err) throw err;
        else {
          if (compareResult == true) {
            let name = matchedUser.firstname + " " + matchedUser.lastname;
            console.log(name);
            resolve(this.bundleResponse(jsonReq, name));
          } else {
            resolve(false);
          }
        }
      });
    });
    return success;
  }

  decodeToken(token) {
    console.log(jwt_secret);
    return jwt.verify(token, jwt_secret);
  }

  bundleResponse(jsonReq, name) {
    let bundle = {
      token: jwt.sign(jsonReq, jwt_secret),
      name,
    };
    console.log(bundle);
    return { bundle };
  }
}
module.exports.UserQuery = UserQuery;
